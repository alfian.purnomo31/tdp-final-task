


import 'assets/css/custom.css';
import 'assets/css/style.css';
import { isLoggedIn } from 'helper';
import Home from 'pages/home';
import Login from 'pages/login';
import MyCart from 'pages/my-cart';
import Profile from 'pages/profile';
import LayoutShop from 'pages/shop';
import Detail from 'pages/shop/detail';
import Shop from 'pages/shop/shop';
import { Navigate, Route, Routes } from 'react-router-dom';
const PrivateRoute = ({children}) => {
    return isLoggedIn() ? children : <Navigate to='/'/>
}
const App = () => {
    return (
        <>

            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path='/shop' element={<PrivateRoute><LayoutShop/></PrivateRoute>}>
                    <Route index element={<Shop/>}/>
                    <Route path='detail/:idProduct' element={<Detail/>} />
                </Route>
                <Route path='/cart' element={<PrivateRoute><MyCart/></PrivateRoute>}></Route>
                <Route path='/login' element={<Login/>}></Route>
            </Routes>
           
        </>
    );
}

export default App;
