import Breadcrumb from "components/Breadcrumb";
import NavBar from "components/NavBar";
import { getToken } from "helper";
import { useState, useEffect } from "react";


const Profile = () => {
    const token = getToken()
    const [dataProfile, setDataProfile] = useState({
        id:null,
        email:'',
        password:'',
        name:'',
        retypePassword:''
    })

    useEffect(()=>{
        
        fetch(`https://api.escuelajs.co/api/v1/auth/profile`,{
            headers:{
                "Authorization": `Bearer ${token}`
            }
        })
        .then(res=>res.json())
        .then(data=>{
            setDataProfile({...data, retypePassword: ''})
            
        })
        .catch(e=>{
            
        })
    },[token])

    const handleSubmit = (e) => {
        
        fetch(`https://api.escuelajs.co/api/v1/users/${dataProfile?.id}`,{
                method:'PUT',
                body: JSON.stringify(dataProfile),
                headers:{
                    Accept: '*/*',
                    'Content-Type': 'application/json'
                }
        })
        .then(data=>{
            console.log({data})
            
        })
        .catch(e=>{
            alert(e)
        })
    }
    
    return (
        <>
            <NavBar/>
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                
                <Breadcrumb
                    data={[
                        {
                            title:'Home',
                            path:'/'
                        },
                        {
                            title:'My Profile',
                            path:''
                        }
                    ]}
                />
                <div className="container padding-32" id="contact" >
                    <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
                    <p>Lets get in touch and talk about your next project.</p>
                    <form>
                        <input onChange={(e)=>{
                            const {value, name} = e.target
                            setDataProfile(prev=>({
                                ...prev,
                                [name]: value
                            }))
                        }} value={dataProfile?.name} className="input section border" type="text" placeholder="Name" name="name" />
                        {
                            dataProfile?.name==="" && <div style={{ color: '#EF144A' }}>Name is required</div>
                        }
                        
                        <input onChange={(e)=>{
                            const {value, name} = e.target
                            setDataProfile(prev=>({
                                ...prev,
                                [name]: value
                            }))
                        }} value={dataProfile?.email} className="input section border" type="text" placeholder="Email" name="email" />
                        {
                            dataProfile?.email==="" && <div style={{ color: '#EF144A' }}>Email is required</div>
                        }
                        
                        <input onChange={(e)=>{
                            const {value, name} = e.target
                            setDataProfile(prev=>({
                                ...prev,
                                [name]: value
                            }))
                        }} value={dataProfile?.password} className="input section border" type="password" placeholder="Password" name="password" />
                        {
                            dataProfile?.password==="" && <div style={{ color: '#EF144A' }}>Password is required</div>
                        }
                        <input onChange={(e)=>{
                            const {value, name} = e.target
                            setDataProfile(prev=>({
                                ...prev,
                                [name]: value
                            }))
                        }} value={dataProfile?.retypePassword} className="input section border" type="password" placeholder="Retype Password" name="retypePassword" />
                        {
                            dataProfile?.retypePassword==="" && <div style={{ color: '#EF144A' }}>Retype Password is required</div>
                        }
                        
                        <button className="button black section" type="button" onClick={handleSubmit}>
                            <i className="fa fa-paper-plane" /> Update
                        </button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default Profile;