import Breadcrumb from "components/Breadcrumb";
import NavBar from "components/NavBar";
import ItemCart from "./_components/ItemCart";

const getMyCart = () => {
    const getCart = localStorage.getItem('CART') 
    const cart = getCart ? JSON.parse(getCart) : []
    return cart
}

const summaryCart = () => {
    const cart = getMyCart()
    let grantTotal = 0;
    let totalPrice = 0;
    if(cart.length > 0){
        grantTotal = cart.map(val=>val.qty*val.price).reduce((prev, current)=>{
            return prev + current
        },0)
    }

    return {
        grantTotal
    }
}
const MyCart = () => {
    const cart = getMyCart()
    const {grantTotal} = summaryCart()
    const handleDelete = (id) => {
        console.log({id})
        const newCart = cart.filter(val=>id!==val.id)
        localStorage.setItem('CART',JSON.stringify(newCart))
        window.location.reload()
    }
    return (
        <>
            <NavBar/>
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <Breadcrumb data={[
                    {
                        title: 'Home',
                        path: '/'
                    },
                    {
                        title: 'My Cart',
                        path: ''
                    }
                ]} />
                <div className="cart-info">
                    <div className="parent__left-side">
                        <div className="shopping-cart">
                            <div className="title">My Cart</div>
                            {
                                cart.length === 0 && <div style={{ textAlign: 'center', margin: '150px', fontWeight: 800 }}>Your Cart is Empty</div>
                            }
                            {
                                cart.length > 0 && cart.map((val,index)=>{
                                    return <ItemCart actionDelete={handleDelete} key={index} data={val} />
                                })
                            }
                            
                            
                        </div>
                    </div>
                    <div className="parent__right-side">
                        <div className='price-summary-cart'>
                            <div className='price-summary-body'>
                                <div>
                                    <h6 className="price-summary-title">Shopping Summary</h6>
                                    <div className="price-summary-detail">
                                        <p className="left">Total Price (items)</p>
                                        <p className="right">Rp {grantTotal}</p>
                                    </div>
                                    <hr className="css-1u5v5t"></hr>
                                    <div className="price-summary-detail">
                                        <h6 className="left">GrandTotal</h6>
                                        <h6 className="right">Rp {grantTotal}</h6>
                                    </div>
                                    <button className='button light-grey block'>
                                        Order
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default MyCart;