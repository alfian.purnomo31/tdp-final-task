const ItemCart = ({data, actionDelete}) => {
    return (
        <div className="flex-container item">
            <div className="buttons">
                <span onClick={()=>actionDelete(data.id)} className="delete-btn"></span>
                <span className="like-btn"></span>
            </div>
            <div className="image">
                <img src={data.images[0]} alt="" style={{ width: '75px' }} />
            </div>
            <div className="description" style={{ width: '50%' }}>
                <span>{data.title}</span>
                <span>{data.description}</span>
            </div>
            <div className="quantity" style={{ width: '25%', textAlign: 'center' }}>
                
                <input type="text" name="name" value="4" readOnly />
                
                
            </div>
            <div className="total-price" style={{ width: '15%' }}>
                <div style={{ fontWeight: 800 }}>
                    Rp {data.price * data.qty}
                </div>
            </div>
        </div>
    )
}

export default ItemCart;