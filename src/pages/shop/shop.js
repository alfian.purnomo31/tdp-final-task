import Breadcrumb from "components/Breadcrumb";
import CardProduct from "components/CardProduct";
import Loading from "components/Loading";
import TitlePage from "components/TitlePage";
import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";

const Shop = () => {
    const [dataProduct, setDataProduct] = useState([])
    const [loadingData, setLoadingData] = useState('idle')
    const [searchParams] = useSearchParams()
    const category = searchParams.get('category')
    useEffect(()=>{
        setLoadingData('pending')
        fetch(`https://api.escuelajs.co/api/v1/products?offset=0&limit=10&${category ? `categoryId=${category}` : ''}`)
        .then(res=>res.json())
        .then(data=>{
            setDataProduct(data)
            setLoadingData('resolved')
        })
        .catch(e=>{
            setLoadingData('rejected')
        })
    },[category])
    return (
        <>
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <Breadcrumb
                    data={[
                        {
                            title: 'Home',
                            path: '/'
                        },
                        {
                            title: 'Shop',
                            path: ''
                        }
                    ]}
                />
                <TitlePage
                    title="All Product"
                />
                {
                    loadingData==="pending" && <Loading/>
                }
                {
                    loadingData==="resolved" && 
                    <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                        
                        {
                            dataProduct.map((val,index)=>{
                                return(
                                    <CardProduct
                                        key={index}
                                        data={val}
                                    />
                                )
                            })
                        }
                    </div>
                }
                
                
            </div>
        </>
    )
}

export default Shop;