import Breadcrumb from "components/Breadcrumb"
import Loading from "components/Loading"
import TitlePage from "components/TitlePage"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"

const Detail = () => {
    const {idProduct} = useParams()
    const [dataProduct, setDataProduct] = useState({})
    const [loadingData, setLoadingData] = useState('idle')
    
    const [quantity, setQuantity] = useState(0)
    useEffect(()=>{
        setLoadingData('pending')
        fetch('https://api.escuelajs.co/api/v1/products/'+ idProduct)
        .then(res=>res.json())
        .then(data=>{
            setDataProduct(data)
            
            setLoadingData('resolved')
        })
        .catch(e=>{
            setLoadingData('rejected')
        })
    },[idProduct])

    const findExisting = () => {
        let newCart = []
        const myCart = JSON.parse(localStorage.getItem('CART'))
        if(myCart){
            const findById = myCart.filter(val=>val.id===dataProduct.id)
            
            if(findById.length > 0){
                newCart = [
                    ...myCart.filter(val=>val.id!==dataProduct.id),
                    {
                        ...findById[0],
                        qty: findById[0].qty + parseInt(quantity)
                    }
                ]
            }else{
                newCart = [
                    ...myCart,
                    {
                        ...dataProduct
                    }
                ]
            }
        }else{
            newCart = [{...dataProduct, qty: parseInt(quantity)}]
        }
        
        return newCart
        
    }
    const handleAddToCart = () => {
        const find = findExisting()
        localStorage.setItem('CART', JSON.stringify(find))
    }
    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            
            <Breadcrumb
                data={[
                    {
                        title: 'Home',
                        path: '/'
                    },
                    {
                        title: 'Shop',
                        path: '/shop'
                    },
                    {
                        title: 'Detail',
                        path: ''
                    }
                ]}
            />
            <TitlePage title="Product Information"/>
            
            {
                loadingData==="pending" && <Loading/>
            }
            {
                loadingData==="resolved" && 
                <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
                    <div className="col l3 m6 margin-bottom">
                        <div className="product-tumb">
                            <img src={dataProduct?.images[0]} alt="Product 1" />
                        </div>
                    </div>
                    <div className="col m6 margin-bottom">
                        <h3>{dataProduct?.title}</h3>
                        <div style={{ marginBottom: '32px' }}>
                            <span>Category : <strong>{dataProduct?.category?.name}</strong></span>
                            
                        </div>
                        <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                            Rp. {dataProduct?.price}
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            {dataProduct?.description}
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            <div><strong>Quantity : </strong></div>
                            <input type="number" value={quantity} className="input section border" name="total" placeholder='Quantity' 
                            onChange={(e)=>{
                                const {value} = e.target
                                setQuantity(value)
                            }}/>
                        </div>
                        <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                            Sub Total : Rp. {quantity * dataProduct?.price}
                            
                        </div>
                        <button onClick={()=>handleAddToCart(dataProduct)} className='button light-grey block' >Add to cart</button>
                    </div>
                </div>
            }
            
        </div>
    )
}

export default Detail