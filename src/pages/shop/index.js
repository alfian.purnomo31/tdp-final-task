import NavBar from "components/NavBar";
import { Outlet } from "react-router-dom";

const LayoutShop = () => {
    return (
        <><NavBar/>
        <Outlet/></>
    )
}

export default LayoutShop;