import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { setToken } from "../../helper";
const Login = () => {
    const [dataForm, setDataForm] = useState({
        email:'',
        password:''
    })
    const navigate = useNavigate()
    const handleSubmit = (e) => {
        e.preventDefault();
        if(dataForm.email && dataForm.password){
            
            fetch('https://api.escuelajs.co/api/v1/auth/login',{
                method:'POST',
                body: JSON.stringify(dataForm),
                headers:{
                    Accept: '*/*',
                    'Content-Type': 'application/json'
                }
            }).then(res=>res.json())
            .then(res=>{
                setToken(res)
                navigate('/shop')
            })
        }
        
    }
    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="contact" >
                <h3 className="border-bottom border-light-grey padding-16">Login</h3>
                <p>Lets get in touch and talk about your next project.</p>
                <form onSubmit={handleSubmit}>
                    <input onChange={(e)=>{
                        setDataForm(prev=>({
                            ...prev,
                            [e.target.name] : e.target.value 
                        }))
                    }} value={dataForm.email} className="input border" type="text" placeholder="email" required name="email" />
                    <input onChange={(e)=>{
                        setDataForm(prev=>({
                            ...prev,
                            [e.target.name] : e.target.value 
                        }))
                    }} value={dataForm.password} className="input section border" type="password" placeholder="Password" required name="password" />
                    <button className="button black section" type="submit">
                        <i className="fa fa-paper-plane" />
                        Login
                    </button>
                </form>
            </div >
        </div>
    )
}

export default Login;