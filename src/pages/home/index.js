import Header from "components/Header";
import NavBar from "components/NavBar";
import SectionCategory from "components/SectionCategory";



const Home = () => {
    return (
        <>

            <NavBar/>            
            <Header/>
            <SectionCategory/>
             <footer className="center black padding-16">
                <p>Copyright 2022</p>
            </footer>
        </>
    )
}

export default Home;