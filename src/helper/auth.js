export const setToken = (param) => {
    localStorage.setItem('TOKEN',param.access_token)
    localStorage.setItem('REFRESH_TOKEN',param.refresh_token)
}

export const getToken = () => {
    return localStorage.getItem('TOKEN') ?? null
}

export const logout = () => {
    localStorage.clear()
}

export const isLoggedIn = () => !!localStorage.getItem('TOKEN')