const TitlePage = ({title}) => {
    return (
        <div className="container padding-32" id="about">
            <h3 className="border-bottom border-light-grey padding-16">
                {title}
            </h3>
        </div>
    )
}

export default TitlePage;