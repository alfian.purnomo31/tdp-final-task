import { Link } from "react-router-dom";
const CardProduct = ({
    data
}) => {
    return (
        <div className="product-card">
            <div className="badge">Discount</div>
            <div className="product-tumb">
                <img src={data.images[0]} alt={data.title} />
            </div>
            <div className="product-details">
                <span className="product-catagory">{data.category.name}</span>
                <h4>
                    <Link to={`/shop/detail/${data.id}`}>{data.title}</Link>
                </h4>
                <p>{data.description}</p>
                <div className="product-bottom-details">
                    <div className="product-price">Rp. {data.price}</div>
                    <div className="product-links">
                        <Link to={`/shop/detail/${data.id}`}>{data.title}</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardProduct;