const Loading = () => {
    return (
        <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
            Loading . . .
        </div>
    )
}

export default Loading;