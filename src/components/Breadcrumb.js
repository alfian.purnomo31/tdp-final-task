import { Link } from "react-router-dom";

const Breadcrumb = ({data}) => {
    
    return (
        <div className="container" style={{ marginTop: '80px' }}>
            <div className='breadcrumb'>
                {
                    data.map((val,index)=>{
                        return val.path ? <Link key={index} to={val.path}>{val.title} {index !== (data.length-1) && <span className="breadcrumb-separator">/</span>}</Link> : <span key={index}>
                        {val.title}
                    </span>
                    })
                }
            </div>
        </div>
    )
}

export default Breadcrumb;