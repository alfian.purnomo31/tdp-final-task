import { getToken, logout } from "helper";

const NavBar = () => {
    const token = getToken()
    return (
        <div className="top" >
            <div className="bar white wide padding card">
                <a href="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</a>
                <div className="right hide-small">
                    <a href="/" className="bar-item button">Home</a>
                    <a href="/shop" className="bar-item button">Shop</a>
                    <a href="/cart" className="bar-item button">My Cart</a>
                    {
                        token ? 
                        <button onClick={()=>{
                            logout()
                        }} className="bar-item button">Logout</button> :
                        <a href="/login" className="bar-item button">Login</a>
                    }
                    
                    
                </div>
            </div>
        </div>
    )
}

export default NavBar;