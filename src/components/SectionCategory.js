import map from 'assets/img/map.jpg';
import { useEffect, useState } from 'react';
const SectionCategory = () => {
    const [dataProduct, setDataProduct] = useState([])
    const [loadingData, setLoadingData] = useState('idle')
    useEffect(()=>{
        setLoadingData('pending')
        fetch(`https://api.escuelajs.co/api/v1/categories`)
        .then(res=>res.json())
        .then(data=>{
            setDataProduct(data.slice(0,4))
            setLoadingData('resolved')
        })
        .catch(e=>{
            setLoadingData('rejected')
        })
    },[])
    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="projects">
                <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
            </div>
            <div className="row-padding">
                {
                    dataProduct.map((val,index)=>{
                        return <div key={index} className="col l3 m6 margin-bottom">
                        <a href={`/shop?category=${val.id}`}>
                            <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                                <div className="display-topleft black padding">{val.name}</div>
                                <img src={val.image} alt={val.name} style={{ maxWidth: '100%', minHeight: '100%' }} />
                            </div>
                        </a>
                    </div>
                    })
                }
            </div>

            <div className="container padding-32">
                <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
            </div>
        </div>
    )
}

export default SectionCategory;